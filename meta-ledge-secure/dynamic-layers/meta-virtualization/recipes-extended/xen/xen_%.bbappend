# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI:append = " \
    file://0001-arm-acpi-don-t-expose-the-ACPI-IORT-SMMUv3-entry-to-.patch \
    file://acpi.cfg \
    file://msi.cfg \
    file://xen.cfg.in"

DEPENDS:append = " gettext-native"

DOM0_MEMORY_SIZE ??= "4096"

inherit image-efi-boot

do_deploy:append() {

    export DOM0_MEMORY_SIZE="${DOM0_MEMORY_SIZE}M"
    export ROOT_PART_UUID="${ROOT_PART_UUID}"

    envsubst < ${WORKDIR}/xen.cfg.in > ${DEPLOYDIR}/xen.cfg
}
