# undo meta-selinux change which fails on kirkstone:
# Feb 23 13:45:47 trs-qemuarm64 mount[788]: mount: /var/volatile: wrong fs type, bad option, bad superblock on tmpfs, missing codepage or helper program, or other error.

REFPOLICY_TYPE = "${@d.getVar('PREFERRED_PROVIDER_virtual/refpolicy').split('-')[1] or ''}"

do_install:append () {
    if [ -n "${REFPOLICY_TYPE}" ]; then
        sed -i -e 's/,rootcontext=system_u:object_r:var_t:s0//g' \
            -e 's/,rootcontext=system_u:object_r:var_t//g' \
            ${D}${sysconfdir}/fstab
    fi
}
